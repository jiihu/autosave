const chartOptions = {
  width: 280,
  height: 150,
  color: '#FFA200',
  margin: {
    top: 20,
    left: 45,
    bottom: 25,
    right: 20
  },
  animate: {
    type: 'delayed',
    duration: 200
  },
  axisX: {
    showAxis: false,
    showLines: true,
    showLabels: false,
    showTicks: true,
    zeroAxis: false,
    orient: 'bottom',
    label: {
      fontFamily: 'Arial',
      fontSize: 14,
      fontWeight: true,
      fill: '#ccc'
    }
  },
  axisY: {
    showAxis: false,
    showLines: true,
    showLabels: true,
    showTicks: false,
    zeroAxis: false,
    orient: 'left',
    label: {
      fontFamily: 'Arial',
      fontSize: 14,
      fontWeight: true,
      fill: '#ccc'
    }
  }
}

export default chartOptions
