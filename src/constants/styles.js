export default styles = {
  break: {
    paddingTop: 20
  },
  container: {
    height: '100%',
    paddingBottom: 20,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 30,
    backgroundColor: "#fff"
  },
  containerNoBackground: {
    height: '100%',
    paddingBottom: 20,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 30
  },
  containerAlignBottom: {
    height: '100%',
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    paddingBottom: 100
  },
  containerBankButton: {
    borderRadius: 100,
    borderWidth: 1.5,
    borderColor: '#d6d7da',
    marginBottom: "3%",
    height: 150,
    width: 150,
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center"
  },
  containerBankButtonImage: {
    width: "80%"
  },
  containerInline: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: "space-around",
    flexWrap: 'wrap'
  },
  containerRadio: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    width: '100%'
  },
  center: {
    alignItems: 'center'
  },
  h1: {
    fontSize: 24,
    paddingBottom: 15,
    paddingTop: 15
  },
  welcomeH1: {
    fontSize: 32,
    marginTop: "20%",
    marginBottom: "10%",
    textAlign: "left",
    alignSelf: "flex-start",
    fontWeight: "bold"
  },
  h2: {
    fontSize: 18,
    paddingBottom: 10,
    paddingTop: 15
  },
  lead: {
    color: '#777',
    fontSize: 16,
    lineHeight: 22,
    paddingBottom: 10
  },
  radio: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  radioTitle: {
  },
  smallPrint: {
    color: '#aaa',
    fontSize: 11,
    paddingTop: 30
  },
  typeDescription: {
    color: '#888',
    fontSize: 14,
    paddingTop: 5,
    textAlign: 'center'
  }
}
