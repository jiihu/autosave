import React from 'react'
import { View, Text, Button, Image } from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import { RkButton } from 'react-native-ui-kitten'
import { RkTextInput } from 'react-native-ui-kitten'

import defaultStyles from '../constants/styles'

const Fifth = ({ goToNext }) =>
  <Image style={{resizeMode: 'contain', width: '100%', height: '100%'}} source={require('../assets/app.png')}>
  </Image>

Fifth.navigationOptions = {
  title: "Fifth",
  header: null
}

const mapDispatchToProps = dispatch => ({
  goToNext: () => dispatch(NavigationActions.navigate({ routeName: 'Main' }))
})

export default connect(null, mapDispatchToProps)(Fifth)
