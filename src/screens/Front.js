
import React, { Component } from 'react';

import { View, Text, Button, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import defaultStyles from '../constants/styles'

import { SmoothLine } from 'react-native-pathjs-charts';

import history from '../data/history';
import chartOptionsFront from '../chartOptionsFront';

import { RkButton } from 'react-native-ui-kitten';

class Front extends Component {
  constructor(props) {
    super(props);
    this.state = {
      savings: 0,
      risks: 3,
      data: [],
      totalEuros: 0
    };
  }

  componentDidMount() {
    this.updateChart();
  }

  updateChart() {
    let years = 5.0;
    let newHistory = [];

    newHistory[0] = history[0].map((obj) => {
      return {"x": (obj.x * years), "y": Math.round(obj.y * years)};
    });

    newHistory[1] = history[0].map((obj) => {
      return {"x": (obj.x * years), "y": Math.round(obj.y * years)};
    });

    // Using 2.5 to make the percentage somewhat correct
    let multiplier = (this.state.risks * 2.5 / 100.0 / (365 / years));
    let totalInterest = 0;

    newHistory[2] = newHistory[0].map((obj) => {
      let interest = (obj.y * multiplier)
      totalInterest += interest;

      let y = Math.round(obj.y + totalInterest);
      return {"x": (obj.x), "y": y};
    });

    this.setState({totalEuros: newHistory[1][newHistory[1].length-1].y})
    this.setState({data: newHistory})
  }

  render() {
    return (
        <View style={[defaultStyles.container, defaultStyles.center]}>
          <Text style={defaultStyles.h1}>
            { "Welcome back Olli!" }
          </Text>

          <Text style={defaultStyles.h2}>
            {"Your savings so far"}
          </Text>
          <SmoothLine data={this.state.data} options={chartOptionsFront} xKey='x' yKey='y' />

        </View>
    );
  }
}

export default connect()(Front)
