import React from 'react'
import { View, Text, Button, Image, TouchableHighlight} from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import { RkButton } from 'react-native-ui-kitten';

import defaultStyles from '../constants/styles'

const First = ({ goToNext }) =>
  <View style={[defaultStyles.container, defaultStyles.center]}>
    <Text style={defaultStyles.welcomeH1}>
      Welcome to AutoSave!
    </Text>
    <Text style={defaultStyles.lead}>
      AutoSave is the easiest way to start saving to funds. Each payment you make with your bank card will increase the amount of your investment in funds.
    </Text>
    <Text style={defaultStyles.h2}>
      What is your bank?
    </Text>

    <View style={defaultStyles.containerInline}>
      <TouchableHighlight style={defaultStyles.containerBankButton} onPress={goToNext}>
        <Image resizeMode="contain" style={defaultStyles.containerBankButtonImage} source={require('../assets/op.png')} />
      </TouchableHighlight>
      <TouchableHighlight style={defaultStyles.containerBankButton} >
        <Image  resizeMode="contain" style={defaultStyles.containerBankButtonImage} source={require('../assets/aktia.png')} />
      </TouchableHighlight>
      <TouchableHighlight style={defaultStyles.containerBankButton} >
        <Image resizeMode="contain" style={defaultStyles.containerBankButtonImage} source={require('../assets/danske.png')} />
      </TouchableHighlight>
      <TouchableHighlight style={defaultStyles.containerBankButton} >
        <Image resizeMode="contain" style={defaultStyles.containerBankButtonImage} source={require('../assets/nordea.png')} />
      </TouchableHighlight>
    </View>
  </View>

/*This doesn't update automatically when language is changed. On willRecieveProps,
can check if language has changed, and set it again acordingly.*/
First.navigationOptions = {
  title: 'First',
  header: null
}

const mapStateToProps = ({ language }) => ({ language })

const mapDispatchToProps = dispatch => ({
  goToNext: () => dispatch(NavigationActions.navigate({ routeName: 'Second' })),
  changeLanguage: language => dispatch(changeLanguage(language))
})

export default connect(mapStateToProps, mapDispatchToProps)(First)
