
import React, { Component } from 'react';

import { View, Text, Button, ScrollView, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import { RkButton } from 'react-native-ui-kitten';
import { RkChoice } from 'react-native-ui-kitten';
import { RkChoiceGroup } from 'react-native-ui-kitten';
import { SmoothLine } from 'react-native-pathjs-charts';

import history from '../data/history';
import chartOptions from '../chartOptions';

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

//import Radiobutton from '../components/Radiobutton'
import defaultStyles from '../constants/styles'

var saving_props = [
  {
    label: (
      <Text style={defaultStyles.radioTitle}>Rounding</Text>
    ),
    value: 0
  },
  {
    label: (
      <Text style={defaultStyles.radioTitle}>Percentage</Text>
    ),
    value: 1
  }
];

var risk_props = [
  {
    label: (
      <Text style={defaultStyles.radioTitle}>No risk</Text>
    ),
    value: 0
  },
  {
    label: (
      <Text style={defaultStyles.radioTitle}>Small</Text>
    ),
    value: 1
  },
  {
    label: (
      <Text style={defaultStyles.radioTitle}>Modest</Text>
    ),
    value: 2
  },
  {
    label: (
      <Text style={defaultStyles.radioTitle}>High</Text>
    ),
    value: 3
  }
];

class Third extends Component {
  constructor(props) {
    super(props);
    this.state = {
      savings: 0,
      risks: 1,
      data: [],
      totalEuros: 0
    };
  }

  componentDidMount() {
    this.updateChart();
  }

  updateChart() {
    let years = 5.0;
    let newHistory = [];

    newHistory[0] = history[0].map((obj) => {
      return {"x": (obj.x * years), "y": Math.round(obj.y * years)};
    });

    newHistory[1] = history[0].map((obj) => {
      return {"x": (obj.x * years), "y": Math.round(obj.y * years)};
    });
    // Using 2.5 to make the percentage somewhat correct
    let multiplier = (this.state.risks * 2.5 / 100.0 / (365 / years));
    let totalInterest = 0;

    newHistory[2] = newHistory[0].map((obj) => {
      let interest = (obj.y * multiplier)
      totalInterest += interest;

      let y = Math.round(obj.y + totalInterest);
      return {"x": (obj.x), "y": y};
    });

    this.setState({totalEuros: newHistory[1][newHistory[1].length-1].y})
    this.setState({data: newHistory})
  }

  handleRisk(value) {
    this.setState({risks: value}, () => this.updateChart() );
  }

  handleSaving(value) {
    this.setState({savings: value});
    this.updateChart();
  }

  render() {
    return (
      <ScrollView bounce={false}>
      <View style={[defaultStyles.container, defaultStyles.center]}>
        <Text style={defaultStyles.h1}>
          Lets get started Olli!
        </Text>

        <View style={defaultStyles.center}>
          <Text style={defaultStyles.typeDescription}>
            {"We will round your every card transaction to the next euro and save the difference. Over 100 euro transactions we will round to the next full 10 euro."}
          </Text>
        </View>


        <View style={defaultStyles.break}></View>
        <Text style={defaultStyles.h2}>
          {"This much you could save in 5 years:"}
        </Text>

        <SmoothLine data={this.state.data} options={chartOptions} xKey='x' yKey='y' />

        <Text style={defaultStyles.h2}>
          How much risk do you want to take?
        </Text>

        <RadioForm
          radio_props={risk_props}
          initial={1}
          formHorizontal={true}
          labelHorizontal={false}
          buttonColor={'#2196f3'}
          animation={true}
          onPress={(value) => {this.handleRisk(value)}}
        />

        <View style={defaultStyles.center}>
          <Text style={defaultStyles.typeDescription}>
            {(() => {
             switch (this.state.risks) {
               case 0: return (
                 "Your savings will go into a regular savings account. Guaranteed interest rate 0.1%."
               );
               case 1: return (
                 "Your savings will go into OP Varovainen fund. Expected interest rate is 1-2%."
               );
               case 2:  return (
                 "Your savings will go into OP Taktinen fund. Expected interest rate is 3-4%."
               );
               case 3:  return (
                 "Your savings will go into OP Rohkea fund. Expected interest rate is 5-6%."
               );
               default: return "d";
             }
           })()}
          </Text>
        </View>

        <View style={defaultStyles.break}></View>
        <Text style={defaultStyles.lead}>
          { "Based on your past transactions, you could save up to " + this.state.totalEuros + " €."}
        </Text>


        <View style={defaultStyles.break}></View>
        <RkButton rkType='styled' onPress={this.props.goToNext}>Start saving</RkButton>

        <Text style={defaultStyles.smallPrint}>
          Past performance of the investment fund is not a guarantee for future results. Investments in funds can increase as well as decrease in value because of market fluctuations, the fund’s risk profile or costs related to subscription, redemption, fund management etc., and the investor may lose the full amount invested.
        </Text>

      </View>
      </ScrollView>
    );
  }
}


Third.navigationOptions = {
  title: 'Third',
  header: null
}

const mapDispatchToProps = dispatch => ({
  goToNext: () => dispatch(NavigationActions.navigate({ routeName: 'Fifth' }))
})

export default connect(null, mapDispatchToProps)(Third)
