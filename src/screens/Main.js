import React from 'react'
import { View, Text, Button } from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions, TabNavigator } from 'react-navigation'

import Front from './Front'
import Monthly from './Monthly'
import Something from './Something'
import Settings from './Settings'

const MainNavigator = TabNavigator({
  Front: { screen: Front, navigationOptions: {title: 'Plans', header: null} },
  Monthly: { screen: Monthly, navigationOptions: {title: 'Recurring', header: null} },
  Settings: { screen: Settings, navigationOptions: {title: 'Settings', header: null} }
}, {
  tabBarPosition: 'bottom',
  tabBarOptions: {
    activeTintColor: '#F5A623',
    labelStyle: {
      fontSize: 14,
      flex: 1,
    },
    style: {
      backgroundColor: 'white'
    }
  }
});

const Main = ({ goToNext }) =>
  <MainNavigator />

Main.navigationOptions = {
  title: 'Main',
  header: null
}

const mapDispatchToProps = dispatch => ({
  goToNext: () => dispatch(NavigationActions.navigate({ routeName: 'Main' }))
})

export default connect(null, mapDispatchToProps)(Main)
