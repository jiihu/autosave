import React, { Component } from 'react';
import { View, Text, Button, Dimensions,StyleSheet,Animated } from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import { RkButton } from 'react-native-ui-kitten'
import { RkTextInput } from 'react-native-ui-kitten'
import Components from 'expo';

import defaultStyles from '../constants/styles'

const videoSource = require('../assets/video.mp4');


class Second extends Component {
  constructor(props) {
    super(props);
    this._fadeInVideo = this._fadeInVideo.bind()
  }

  state = {
   loaded: false,
   backgroundOpacity: new Animated.Value(0),
  }

  async componentWillMount() {
     this.setState({loaded: true});
  }
  _fadeInVideo = () => {
   setTimeout(() => {
     Animated.spring(this.state.backgroundOpacity, {toValue: 1}).start()
   }, 500);
 }

  render() {
    return (
      <View style={defaultStyles.containerNoBackground}>
        <Text style={defaultStyles.h1}>
          Log in
        </Text>
        <Animated.View style={[styles.backgroundViewWrapper, {opacity: this.state.backgroundOpacity}]}>
          <Components.Video
                source={videoSource}
                style={{width: Dimensions.get('window').width, height: Dimensions.get('window').height}}
                resizeMode="cover"
                shouldPlay
                repeat
                mute={true}
                onLoad={() => this._fadeInVideo()}
          />
        </Animated.View>
        <View style={defaultStyles.containerAlignBottom}>
          <RkTextInput rkType='opInput' secureTextEntry={true}
            labelStyle={{color: '#fff', paddingLeft: 10}}
            label='OP username' />
          <RkTextInput rkType='opInput' secureTextEntry={true}
            labelStyle={{color: '#fff', paddingLeft: 10}}
            label='OP password' />
          <RkButton rkType='styled' onPress={this.props.goToNext}>Log in</RkButton>
        </View>
      </View>
    )
 }
}

Second.navigationOptions = {
  title: "Second",
  header: null
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  background: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#000',
  },
  backgroundViewWrapper: {
    ...StyleSheet.absoluteFillObject,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  title: {
    fontSize: 50,
    color: '#fff',
    textAlign: 'center',
    marginTop: 50,
  },
});


const mapDispatchToProps = dispatch => ({
  goToNext: () => dispatch(NavigationActions.navigate({ routeName: 'Third' }))
})

export default connect(null, mapDispatchToProps)(Second)
