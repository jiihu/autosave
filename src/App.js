import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addNavigationHelpers, StackNavigator } from 'react-navigation'

import { RkTheme } from 'react-native-ui-kitten'

RkTheme.setType('RkButton', 'bank', {
  marginBottom: 5,
  marginRight: 5,
  paddingBottom: 15,
  paddingTop: 15
});

RkTheme.setType('RkButton', 'styled', {
  backgroundColor: '#F5A623',
  borderRadius: 30
});

RkTheme.setType('RkTextInput', 'rounded', {
  paddingLeft: 15,
  paddingRight: 15
});

RkTheme.setType('RkTextInput', 'opInput', {
  input: {
    borderColor: '#fff'
  },
  borderColor: '#fff',
  color: '#fff',
  paddingLeft: 15,
  paddingRight: 15
});

//Screens
import First from './screens/First'
import Second from './screens/Second'
import Third from './screens/Third'
import Fifth from './screens/Fifth'
import Main from './screens/Main'

export const AppNavigator = StackNavigator({
  First: {
    screen: First
  },
  Second: {
    screen: Second
  },
  Third: {
    screen: Third
  },
  Fifth: {
    screen: Fifth
  },
  Main: {
    screen: Main
  }
})



const App = ({ dispatch, nav }) =>
  <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired
}

const mapStateToProps = ({ nav }) => ({
  nav
})

export default connect(mapStateToProps)(App)
